plugins {
    `kotlin-dsl`
}

kotlin {
    jvmToolchain {
        val javaToolchainSpec = this as JavaToolchainSpec
        javaToolchainSpec.languageVersion.set(JavaLanguageVersion.of(BuildJvm.version))
        javaToolchainSpec.vendor.set(JvmVendorSpec.AMAZON)
    }
}

repositories {
    mavenCentral()
}

/*
  This object is only visible within this file.
  See Dependencies.kt for Jvm.version
  Ensure to have the same value in both places.
 */
object BuildJvm {
    const val version = 17
}