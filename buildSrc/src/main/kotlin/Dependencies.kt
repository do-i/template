object Application {
    const val mainClass = "com.djd.gig.template.Main"
    const val groupGroup = "com.djd.fun"
    const val version = "2.0.0"
    const val description = "Application Template"
}

object Plugins {
    object Jacoco {
        // https://mvnrepository.com/artifact/org.jacoco/org.jacoco.agent
        const val version = "0.8.7" // As of 2022.01.22
        const val reportDir = "customJacocoReportDir"
    }

    object Format {
        // https://mvnrepository.com/artifact/com.google.googlejavaformat/google-java-format
        const val version = "1.13.0" // As of 2022.01.22
    }

    object Spotless {
        // https://mvnrepository.com/artifact/com.diffplug.spotless/spotless-plugin-gradle
        const val version = "6.2.0" // As of 2022.01.22
        const val name = "com.diffplug.spotless"
    }
}

object Jvm {
    /*
      This object is only visible within this file.
      See build.gradle.kt for BuildJvm.version
      Ensure to have the same value in both places.
     */
    const val version = 17

    object Options {
        const val enablePreview = "--enable-preview"
        const val lintPreview = "-Xlint:preview"
    }
}

object Dependencies {

    object Logback {
        private const val version = "1.2.10" // As of 2022.02.22
        const val core = "ch.qos.logback:logback-core:$version"
        const val classic = "ch.qos.logback:logback-classic:$version"
    }

    object Slf4j {
        private const val version = "1.7.36" // As of 2022.02.22
        const val simple = "org.slf4j:slf4j-simple:$version"
        const val api = "org.slf4j:slf4j-api:$version"
    }

    object Guava {
        private const val version = "31.0.1-jre" // As of 2022.01.22
        const val guava = "com.google.guava:guava:$version"
    }

    object Test {
        object Junit {
            private const val version = "5.8.2" // As of 2022.01.22
            const val junit_engine = "org.junit.jupiter:junit-jupiter-engine:$version"
            const val junit_params = "org.junit.jupiter:junit-jupiter-params:$version"
        }

        object Assertj {
            private const val version = "3.22.0" // As of 2022.01.22
            const val assertj = "org.assertj:assertj-core:$version"
        }

        object Mockito {
            private const val version = "4.2.0" // As of 2022.01.22
            const val mockito_core = "org.mockito:mockito-core:$version"
            const val mockito_junit = "org.mockito:mockito-junit-jupiter:$version"
        }
    }
}