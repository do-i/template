plugins {
    `kotlin-dsl`
    application
    jacoco
    java
    id(Plugins.Spotless.name) version Plugins.Spotless.version
}

application {
    mainClass.set(Application.mainClass)
}

/**
 * Configure jacoco plugin
 */
jacoco {
    toolVersion = Plugins.Jacoco.version
    reportsDirectory.set(layout.buildDirectory.dir(Plugins.Jacoco.reportDir))
}

java {
    toolchain {
        languageVersion.set(JavaLanguageVersion.of(Jvm.version))
        vendor.set(JvmVendorSpec.AMAZON)
    }
}

kotlin {
    jvmToolchain {
        val javaToolchainSpec = this as JavaToolchainSpec
        javaToolchainSpec.languageVersion.set(JavaLanguageVersion.of(Jvm.version))
        javaToolchainSpec.vendor.set(JvmVendorSpec.AMAZON)
    }
}

/**
 * Configure spotless plugin
 */
spotless {
    java {
        googleJavaFormat(Plugins.Format.version)
        removeUnusedImports()
    }
}

repositories {
    mavenCentral()
}

group = Application.groupGroup
version = Application.version
description = Application.description

dependencies {

//    implementation(Dependencies.Slf4j.api)
//    implementation(Dependencies.Slf4j.simple)
    implementation(Dependencies.Logback.classic)

    compileOnly(gradleApi())

    implementation(Dependencies.Guava.guava)

//    testImplementation(Dependencies.Logback.classic)
    testImplementation(Dependencies.Test.Assertj.assertj)
    with(Dependencies.Test.Junit) {
        testImplementation(junit_engine)
        testImplementation(junit_params)
    }
    with(Dependencies.Test.Mockito) {
        testImplementation(mockito_core)
        testImplementation(mockito_junit)
    }
}

tasks.check {
    dependsOn(tasks.jacocoTestCoverageVerification)
}

tasks.withType<JacocoCoverageVerification> {
    violationRules {
        rule {
            limit {
                counter = "LINE"
                value = "TOTALCOUNT"
                minimum = "1.0".toBigDecimal()
            }
        }
    }
}

tasks.withType<JacocoReport> {
    dependsOn(tasks.test) // tests are required to run before generating the report
}

tasks.withType<JavaCompile> {
    dependsOn(tasks.spotlessApply)
    options.compilerArgs.add(Jvm.Options.enablePreview)
    options.compilerArgs.add(Jvm.Options.lintPreview)
}

tasks.withType<JavaExec> {
    /*
     Without `?.` operator the following error is thrown
     Only safe (?.) or non-null asserted (!!.) calls are allowed on a nullable receiver of type (Mutable)List<String!>?
    */
    jvmArgs?.add(Jvm.Options.enablePreview)
}

tasks.withType<Test> {
    useJUnitPlatform()
    finalizedBy(tasks.jacocoTestReport)  // report is always generated after tests run
    jvmArgs(Jvm.Options.enablePreview)
}
