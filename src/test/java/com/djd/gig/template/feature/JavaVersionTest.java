package com.djd.gig.template.feature;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;

import org.junit.jupiter.api.Test;

class JavaVersionTest {

  @Test
  void getVersion() {
    assertThat(new JavaVersion().getMajorVersion()).isEqualTo("17");
  }
}
