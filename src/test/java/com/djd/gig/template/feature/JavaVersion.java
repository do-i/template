package com.djd.gig.template.feature;

public class JavaVersion {

  private final String version;

  public JavaVersion() {
    version = System.getProperty("java.version", "Unknown");
  }

  public String getVersion() {
    return version;
  }

  public String getMajorVersion() {
    var tokens = version.split("\\.");
    return tokens.length != 0 ? tokens[0] : "N/A";
  }
}
