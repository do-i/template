package com.djd.gig.template;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

class FruitsTest {

  private Fruits fruits;

  @BeforeEach
  void setUp() {
    fruits = new Fruits();
  }

  @Test
  void giveMeFruits() {
    assertThat(fruits.giveMeFruits()).containsExactly("Apple", "Banana", "Cantaloupe", "Durian");
  }
}
