package com.djd.gig.template;

import com.djd.gig.template.feature.User;
import com.djd.gig.template.feature.User.PowerUser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Main {

  private static final Logger log = LoggerFactory.getLogger(Main.class);

  private Main() {}

  public static void main(String[] args) {
    var msg = User.withSealedClass(new PowerUser());
    System.out.println(msg);
    new Fruits().giveMeFruits().forEach(log::info);
    log.warn("============> Hello 1a <==============");
    log.info("============> Hello 2a <==============");
  }
}
