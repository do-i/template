package com.djd.gig.template.feature;

import java.time.DayOfWeek;

public class MoreSwitch {

  public static boolean isWeekday(DayOfWeek dayOfWeek) {
    /*
      switch as an expression.
    */
    return switch (dayOfWeek) {
      case MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY -> true;
      case SATURDAY, SUNDAY -> false;
    };
  }
}
