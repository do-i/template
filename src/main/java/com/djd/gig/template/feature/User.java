package com.djd.gig.template.feature;

/** sealed class (Java 17) */
public sealed interface User {
  final class AdminUser implements User {}

  final class PowerUser implements User {}

  final class BasicUser implements User {}

  /** Pattern matching switch expression (Java 17 Preview) */
  static String withSealedClass(User user) {
    return switch (user) {
      case AdminUser ignored -> "Can do anything.";
      case PowerUser ignored -> "Can do powerful stuff.";
      case BasicUser ignored -> "Can do limited things.";
    };
  }
}
