package com.djd.gig.template.feature;

public class MoreInstanceOf {

  // Class casting as part of type checking
  public void doSomething(Number number) {
    if (number instanceof Integer intNum) {
      int value = intNum;
    } else if (number instanceof Long longNum) {
      long value = longNum;
    }
  }
}
