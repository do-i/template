package com.djd.gig.template.feature;

/** Text Block (Java 13) */
public class MoreText {

  /** https://patorjk.com/software/taag/#p=display&f=Georgia11&t=fun.djd.com */
  public static final String WALL_OF_ART =
      """

                                                                                                            \s
                ,...                                ,,   ,,        ,,                                       \s
              .d' ""                              `7MM   db      `7MM                                       \s
              dM`                                   MM             MM                                       \s
             mMMmm`7MM  `7MM  `7MMpMMMb.       ,M""bMM `7MM   ,M""bMM      ,p6"bo   ,pW"Wq.`7MMpMMMb.pMMMb. \s
              MM    MM    MM    MM    MM     ,AP    MM   MM ,AP    MM     6M'  OO  6W'   `Wb MM    MM    MM \s
              MM    MM    MM    MM    MM     8MI    MM   MM 8MI    MM     8M       8M     M8 MM    MM    MM \s
              MM    MM    MM    MM    MM  ,, `Mb    MM   MM `Mb    MM  ,, YM.    , YA.   ,A9 MM    MM    MM \s
            .JMML.  `Mbod"YML..JMML  JMML.db  `Wbmd"MML. MM  `Wbmd"MML.db  YMbmd'   `Ybmd9'.JMML  JMML  JMML.
                                                      QO MP                                                 \s
                                                      `bmP                                                  \s

      """;
}
