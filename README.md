# template for typical build.gradle script

## buildscript block

First section is `buildscript` block. This block must come first.
In here define all variables with `ext` prefix.

## plugins block

Second section is `plugins` block. There are two main plugins.
One is `jacoco` plugin. This is test coverage checker.
The other one is `google-java-format` plugin to format code.

## Automatic task run

Applies code format before compileJava task

    compileJava.dependsOn format

Creates Html format of jacoco test coverage report in `reports/jacoco` at the end of the test.
    
    test.finalizedBy(jacocoTestReport)

Fails build if test coverage is below the limit. See jacocoTestCoverageVerification task block.
    
    check.dependsOn(jacocoTestCoverageVerification)
